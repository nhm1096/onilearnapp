package com.onilearnapp.onilearnapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onilearnapp.onilearnapp.R;

import java.util.List;

public class AnswerAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> answerList;

    public AnswerAdapter(@NonNull Context context, @NonNull List<String> objects) {
        super(context, 0, objects);
        this.context = context;
        this.answerList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String content = answerList.get(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_answer, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.txtAnswer);
        textView.setText(content);

        return convertView;

    }
}
