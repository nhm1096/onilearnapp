package com.onilearnapp.onilearnapp.Model;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {
    private int id;
    private String content;
    private int typeQuestion;
    private List<String> answers;

    public Question(String content, int typeQuestion) {
        this.content = content;
        this.typeQuestion = typeQuestion;
    }

    public Question(int id, String content, int typeQuestion, List<String> answers) {
        this.id = id;
        this.content = content;
        this.typeQuestion = typeQuestion;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTypeQuestion() {
        return typeQuestion;
    }

    public void setTypeQuestion(int typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
