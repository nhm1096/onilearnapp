package com.onilearnapp.onilearnapp.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.onilearnapp.onilearnapp.Adapter.AnswerAdapter;
import com.onilearnapp.onilearnapp.Model.Question;
import com.onilearnapp.onilearnapp.R;
import com.onilearnapp.onilearnapp.Utils.CustomDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MulQuestionActivity extends AppCompatActivity {

    CustomDialog customDialog;
    boolean isClicked;
    ListView listView;
    List<Question> questionList;

    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mul_question);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setTitle("");

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        isClicked = false;

        listView = (ListView) findViewById(R.id.questionListView);

        //get data list answer of question
        List<String> answers = new ArrayList<>();

        answers.add("A: 11");
        answers.add("B: 22");
        answers.add("C: 33");
        answers.add("D: 44");

        ///get data question
        Question question = new Question(1, "Simple question ?", 1, answers);
        TextView title = (TextView) findViewById(R.id.txtTitle);
        title.setText(question.getContent());


        questionList = new ArrayList<>();
        questionList.add(question);


        ArrayAdapter<String> arrayAdapter = new AnswerAdapter(this, answers);
        listView.setAdapter(arrayAdapter);
        listView.setDivider(null);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                LinearLayout layout = (LinearLayout) view.findViewById(R.id.layoutAnswer);
                if(!isClicked) {
                    layout.setBackgroundResource(R.drawable.round_corner_green);
                } else {
                    layout.setBackgroundResource(R.drawable.round_corner);
                }

                isClicked = !isClicked;

            }
        });

        submit = (Button) findViewById(R.id.submitButton);
        submit.setText("SUBMIT");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSubmitClick(View view) {
        if(isClicked) {
            //customDialog = new CustomDialog(this, "", R.drawable.true_icon);
            //customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //customDialog.setCanceledOnTouchOutside(false);
            //customDialog.show();

            Intent intent = new Intent(getApplicationContext(), AnswerActivity.class);
            intent.putExtra("answer_full", (Serializable) questionList);
            startActivity(intent);
        }
    }
}
