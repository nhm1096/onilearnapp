package com.onilearnapp.onilearnapp.Activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.onilearnapp.onilearnapp.Adapter.AnswerFullAdapter;
import com.onilearnapp.onilearnapp.Model.Question;
import com.onilearnapp.onilearnapp.R;

import java.util.ArrayList;
import java.util.List;

public class AnswerActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setTitle("");

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        setupAnswerList();
    }

    void setupAnswerList() {
        listView = (ListView) findViewById(R.id.answerListView);

        Question question = new Question(1, "Question 1: Simple question ?", 1, new ArrayList<String>());
        Question question1 = new Question(2, "Question 2: Simple question ?", 0, new ArrayList<String>());
        Question question2 = new Question(3, "Question 3: Simple question ?", 1, new ArrayList<String>());
        Question question3 = new Question(4, "Question 4: Simple question ?", 0, new ArrayList<String>());
        Question question4 = new Question(5, "Question 5: Simple question ?", 1, new ArrayList<String>());

        List<Question> questionList = new ArrayList<>();
        questionList.add(question);
        questionList.add(question1);
        questionList.add(question2);
        questionList.add(question3);
        questionList.add(question4);

        ArrayAdapter<Question> questionArrayAdapter = new AnswerFullAdapter(this, questionList);

        listView.setAdapter(questionArrayAdapter);
        listView.setDivider(null);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
